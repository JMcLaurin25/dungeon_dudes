from sys import exit
import levels as lvls
import messages as msg


class Engine:

    """Runs the game in a loop until complete,
    Establishes the level selection."""

    def __init__(
            self, level):
        self._level = level

    def play(
            self):
        """Begins at first level and advances to returned
        next level"""
        cur_level = self._level.begin_game()

        while True:
            next_lvl_name = cur_level.begin()
            cur_level = self._level.next_level(next_lvl_name)


class Map:

    """The Map class stores the levels of the the game and the
    subsequent pathways associated with each level"""

    def __init__(
            self, hero, level):
        m = msg.Messages()
        self._level01 = level
        self._hero = hero
        self._levels = {
            'Living Room': lvls.NewLevel(
                self._hero,
                0,
                0,
                m.house1,
                m.house2,
                'bedroom',
                'forest'),
            'bedroom': lvls.NewLevel(
                self._hero,
                0,
                2,
                m.room1,
                m.room2,
                'Living Room'),
            'forest': lvls.NewLevel(
                self._hero,
                0,
                0,
                m.forest1,
                m.forest2,
                'bushes',
                'Cave Entrance'),
            'bushes': lvls.NewLevel(
                self._hero,
                0,
                1,
                m.bush1,
                m.bush2,
                'forest'),
            'Cave Entrance': lvls.NewLevel(
                self._hero,
                0,
                0,
                m.entry1,
                m.entry2,
                'Entry Tunnel'),
            'Entry Tunnel': lvls.NewLevel(
                self._hero,
                1,
                0,
                m.tunnel1,
                m.tunnel2,
                'Left Entry Opening',
                'Cave Tunnel',
                'Right Entry Opening'),
            'Left Entry Opening': lvls.NewLevel(
                self._hero,
                0,
                1,
                m.deadend1,
                m.deadend2,
                'Entry Tunnel'),
            'Right Entry Opening': lvls.NewLevel(
                self._hero,
                0,
                1,
                m.deadend1,
                m.deadend2,
                'Entry Tunnel'),
            'Cave Tunnel': lvls.NewLevel(
                self._hero,
                1,
                0,
                m.tunnel1,
                m.tunnel2,
                'Tunnel Opening',
                'Muggy Opening',
                'Dark Cavern'),
            'Tunnel Opening': lvls.NewLevel(
                self._hero,
                0,
                1,
                m.deadend1,
                m.deadend2,
                'Cave Tunnel'),
            'Muggy Opening': lvls.NewLevel(
                self._hero,
                1,
                0,
                m.lake1,
                m.lake2,
                'Cross Lake'),
            'Cross Lake': lvls.NewLevel(
                self._hero,
                0,
                2,
                m.lakeExit1,
                m.lakeExit2,
                'Lake Opening',
                'Dark Cavern'),
            'Lake Opening': lvls.NewLevel(
                self._hero,
                0,
                1,
                m.deadend1,
                m.deadend2,
                'Dark Cavern'),
            'Dark Cavern': lvls.NewLevel(
                self._hero,
                1,
                0,
                m.tunnel1,
                m.tunnel2,
                'Cavern opening',
                'Glowing Cavern'),
            'Cavern opening': lvls.NewLevel(
                self._hero,
                0,
                1,
                m.deadend1,
                m.deadend2,
                'Dark Cavern'),
            'Glowing Cavern': lvls.BossLevel(
                self._hero,
                'DayLight'),
            'DayLight': lvls.NewLevel(
                self._hero,
                0,
                0,
                "",
                "")
        }

    def next_level(self, level_name):
        return self._levels.get(level_name, 'Level not found')

    def begin_game(self):
        return self.next_level(self._level01)
