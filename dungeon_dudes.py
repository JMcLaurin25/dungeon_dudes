#!/usr/bin/python3

import game_engine as ge
import character as ch
from os import system as sys


def main():
    """Initial start point for Dungeon dudes. Creates a Hero(), the Map(),
    the Engine(), and then starts the game"""
    sys('clear')
    print("\n\t -- Welcome to Dungeon Dudes. --\n")
    name = input("\tWhat's your name? ")
    gender = input("\tAre you a man or a woman? ")
    sys('clear')
    hero = ch.Hero(name, gender, 'Human')

    welcome = "\n\tPleased to meet you"
    if gender.lower() == "man":
        welcome += " Mr. "
    elif gender.lower() == "woman":
        welcome += " Miss "
    else:
        welcome += " 'Whatever you are' "
    welcome += name
    welcome += ". You currently find yourself \n"
    welcome += "standing in the living room of your small"
    welcome += " apartment, unable to connect to the \n"
    welcome += "internet. You find a letter on the floor "
    welcome += "that reads 'I stole your router and \n"
    welcome += "hid it away in a dark cavern in a cold dungeon'."

    print(welcome)
    input("\n<enter to continue>")
    sys('clear')
    print("\nTo reconnect to the Internet you have to retrieve the router.")
    input("\n<enter to continue>")
    sys('clear')

    game_map = ge.Map(hero, 'Living Room')
    game = ge.Engine(game_map)
    game.play()

if __name__ == "__main__":
    main()
