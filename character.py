import random
from os import system as sys
from time import sleep


class Character:

    """Defines the characteristics and functions for a character"""

    def __init__(self, species, *values):
        self._species = species
        self._health = values[0]
        self._level = values[1]
        self._defense = 0

    @property
    def species(self):
        return self._species

    @property
    def health(self):
        return self._health

    @property
    def level(self):
        return self._level

    @property
    def defense(self):
        return self._defense

    @property
    def roll_dice(self):
        return random.randint(1, 6)

    def attack(self, other):
        if self.species == 'Human':
            while True:
                input_statement = "\n\tS - Show character Info\n\tE -"
                input_statement += " Equip/Use Item, an other key to "
                input_statement += "continue\n\n: "
                attack_input = input(input_statement)
                if attack_input.lower() == 's':
                    print(self)
                    print("\n")
                    print(other)
                elif attack_input.lower() == 'e':
                    if self._inventory == []:
                        print("\t - You have nothing to equip")
                    else:
                        damage = self.equip(1)
                        other._health -= damage
                        print("\n\t{0} was hit with {1} damage".format(
                            other.species,
                            damage))
                        if (other._health < 1):
                            break
                        continue
                else:
                    break
        self_rolls = []
        other_rolls = []

        sleep(1)
        print("\n{0} attacks {1}".format(self.species, other.species))
        for i in range(0, 3):
            self_rolls.append(self.roll_dice)
            other_rolls.append(other.roll_dice)
        if (max(self_rolls) > max(other_rolls)):
            damage = (((self.level * 4) - (other.defense * 2)) % 100)
            other._health -= damage
            print("\n\t{0} hits {1} with {2} damage".format(
                self.species, other.species, damage))
            if (other.health < 1):
                return True
            print("\n{0} has {1} health remaining".format(
                other.species, other.health))
            return True
        else:
            print("\nMissed attack!")
            return False


class Hero(Character):

    """Hero object. Child of Character class. Name, Gender,
    Species, and initial level of 1"""

    def __init__(self, name, gender, species, health=100, level=2):
        super().__init__(species, health, level)
        self._name = name
        self._gender = gender
        self._inventory = []

    def add_item(self, item):
        self._inventory.append(item)

    def equip(self, fight=0):
        """Lists inventory for player and performs actions on item"""
        while True:
            input_statement = "    Select Item : "
            index = 1
            for item in self._inventory:
                input_statement += "({0})-{1} ".format(str(index), item.item)
                index += 1
            input_statement += "\n: "

            selection = input(input_statement)
            if (selection.isnumeric() == False):
                continue

            if int(selection) < 1 or int(selection) > len(self._inventory):
                continue

            item = self._inventory[int(selection) - 1]

            if item.item == 'Sheild' or item.item == 'Sword':
                self._level += item.attack
                self._defense += item.defense
                self._inventory.remove(item)
                return True
            if item.item == 'Potion':
                self._health += item.defense
                self._inventory.remove(item)
                return True
            if item.item == 'Bomb':
                if fight == 1:
                    attack = item.attack
                    self._inventory.remove(item)
                    return attack
                return False

            print("Invalid selection")

    def __str__(self):
        """Output of the Heros information"""
        sys('clear')
        output = "\n\t{0:10}: {1:>15}\t".format('Character', self._name)
        output += "{0:10}: {1:>15}\n".format('Gender', self._gender)
        output += "\t{0:10}: {1:>15}\t".format('Species', self._species)
        output += "{0:10}: {1:>15}\n".format('Health', self._health)
        output += "\t{0:10}: {1:>15}\t".format('Level', self._level)
        output += "{0:10}: {1:>15}\n".format('Defense', self._defense)
        output += "\t{0:10}: ".format('Inventory')
        for item in self._inventory:
            output += "({0})".format(item.item)
        return output


class Monster(Character):
    monsters = {
        'Troll': [15, 2], 'Goblin': [10, 3], 'Ogre': [15, 2]
    }

    def __init__(self):
        cur_monst = random.choice(list(Monster.monsters.keys()))
        values = Monster.monsters[cur_monst]
        super().__init__(cur_monst, values[0], values[1])

    @property
    def species(self):
        return self._species

    def __str__(self):
        """Output of the Heros information"""
        output = "    {0:10}: {1:<8}".format('Species', self.species)
        output += "{0:10}: {1:<8}\n".format('Health', self.health)
        output += "    {0:10}: {1:<8}".format('Level', self.level)
        output += "{0:10}: {1:<8}\n".format('Defense', self.defense)
        return output


class Treasure:

    """Creates a treasure item and sets attributes for the item"""
    treasures = {
        'Sheild': [0, 3], 'Sword': [3, 0], 'Bomb': [15, 0], 'Potion': [0, 15]
    }

    def __init__(self):
        cur_trsr = random.choice(list(Treasure.treasures.keys()))
        values = Treasure.treasures[cur_trsr]
        self._item = cur_trsr
        self._attack = values[0]
        self._defense = values[1]

    @property
    def item(self):
        return self._item

    @property
    def attack(self):
        return self._attack

    @property
    def defense(self):
        return self._defense

    def __str__(self):
        output = "{0}: {1:<9}".format('Item', self._item)
        output += "{0}: {1:<5}".format('Attack', self.attack)
        output += "{0}: {1:<5}".format('Defense', self.defense)
        return output
