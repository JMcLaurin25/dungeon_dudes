
class Messages:

    """This class contains all the levels storyline messages
    that will be passed my the game engine."""

    def __init__(self):
        self.house1 = "\nYou are at the house and you can begin by going"
        self.house1 += " to the bedroom or heading out the door to the forest."
        self.house2 = ""

        self.room1 = "\nYou have entered the Bedroom"
        self.room2 = "\nThere is nothing else in the bedroom"

        self.forest1 = "\nNow you are on a dirt path in the forest"
        self.forest2 = ""

        self.bush1 = "\nYou are now stuck in a small burrow in the woods"
        self.bush2 = "\nThere is nothing else in this burrow"

        self.entry1 = "\nThe caves entrance is cool with dim lighting"
        self.entry2 = ""

        self.exit1 = "\nFinally, You have arrived at the tunnels Exit!"
        self.exit2 = "\n\tCONGRATULATIONS! You have retrieved the Golden"
        self.exit2 += " Router and have found the exit to the dungeon."
        self.exit2 += " Game WON!"

        self.deadend1 = "\nOh no, you have found a dead end!"
        self.deadend2 = "\nThere is nothing else in this dark and cold hole"

        self.lake1 = "\nYou have just stumbled upon a huge underground"
        self.lake1 += " lake and the only way out of here is to cross it!"
        self.lake2 = ""

        self.lakeExit1 = "\nYou have successfully crossed the lake but"
        self.lakeExit1 += " doing so has weakened you and lowered your level"
        self.lakeExit2 = "\nThere is nothing else in the room"

        self.tunnel1 = "\nYou've found yourself in a dark muggy tunnel"
        self.tunnel2 = "\nWhere will you go now?"
