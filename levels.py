import character as ch
from os import system as sys
from random import randint


class Level:

    """The Level class initializes the monsters, level
    entrance message, treasures, and paths"""

    def __init__(self, hero, monsters=0, treasures=0):
        """Monsters, Treasures, paths"""
        self._hero = hero
        self._monsters = [ch.Monster() for monster in range(0, monsters)]
        self._lvlMsg = ""
        self._treasure = [ch.Treasure() for treasure in range(0, treasures)]
        self._paths = []
        self._visits = 0

    def user_select(self):
        """Presents the path selection to the player and excepts the choice"""
        while True:
            index = 1
            input_statement = "\n\tS - Show character Info\n\tE - Equip Item\n"
            for path in self._paths:
                input_statement += "\t{0} - {1}\n".format(
                    str(index), path.upper())
                index += 1
            input_statement += "\nSelection: "

            selection = input(input_statement)
            if selection.lower() == 's':
                print(self._hero)
                continue
            if selection.lower() == 'e':
                if self._hero._inventory == []:
                    print("You have nothing to equip")
                    continue
                if self._hero.equip() == False:
                    print("Item cannot be equipped")
                continue
            if (selection.isnumeric() == False):
                continue

            if int(selection) >= 1 and int(selection) <= len(self._paths):
                return self._paths[int(selection) - 1]

            print("Invalid selection")

    def enter_room(self):
        if self._monsters != [] and self._visits == 0:
            pass
            self.fight()

        if self._treasure != [] and self._visits == 0:
            self.collect()

    def fight(self):
        for monster in self._monsters:
            hero_rolls = []
            mnst_rolls = []

            print("You have encountered an " + monster.species +
                  ". Roll dice to determine who attacks first.")

            input("\n<press enter to roll>")
            for i in range(0, 3):
                hero_rolls.append(self._hero.roll_dice)
                mnst_rolls.append(monster.roll_dice)

            results = "\n      Your rolls: {0} - Highest = {1}\n".format(
                hero_rolls, max(hero_rolls))
            results += "  Monsters rolls: {0} - Highest = {1}\n".format(
                mnst_rolls, max(mnst_rolls))
            print(results)

            if (max(hero_rolls) > max(mnst_rolls)):
                first = self._hero
                second = monster
            else:
                first = monster
                second = self._hero

            print("{0} won the roll".format(first.species))
            input("\n<enter to continue>")
            while (monster.health > 0 and self._hero.health > 0):
                first.attack(second)
                if (second.health < 1):
                    print("\n\t{0} killed!".format(second.species))
                    break

                if second.species == 'Human':
                    input("\n<enter to continue>")
                second.attack(first)
                if (first.health < 1):
                    print("\n\t{0} killed!".format(first.species))
                    break

                input("\n<enter to continue>")
                sys('clear')

            if monster.health <= 0:
                print("\nYou have defeated the monster!")

            elif self._hero.health <= 0:
                print("\nYou were defeated!")
                exit(1)

    def collect(self):
        """Presents the user with new treasure to be collected"""
        while True:
            print("You have found treasure. What will you take?\n")
            item_num = 1
            output = ""
            for item in self._treasure:
                output += "\t({0}) - {1}\n".format(item_num, item)
                item_num += 1
            output += "\nSelection: "
            selection = input(output)
            if (selection.isnumeric() == False):
                continue

            if int(selection) >= 1 and int(selection) <= len(self._treasure):
                self._hero.add_item(self._treasure[int(selection) - 1])
                self._treasure.pop(int(selection) - 1)
                break

    def __str__(self):
        output = "{0:10}:\n".format('Monsters')
        for monster in self._monsters:
            output += "{0:>15}\n".format(monster.species)
        output += "{0:10}:\n".format('Treasure', self._treasure)
        for item in self._treasure:
            output += "{0:>15}\n".format(item.item)
        output += "{0:10}: {1}\n".format('Paths', self._paths)
        return output


class NewLevel(Level):

    """Child to class Level, establishes the functions,
    traits, and subsequent level to this particular level"""

    def __init__(self, hero, monsters, treasures, msg1, msg2, *nxt_lvl):
        super().__init__(hero, monsters, treasures)
        self._lvlMsg = msg1
        self._lvlMsg2 = msg2
        for level in nxt_lvl:
            self._paths.append(level)

    def begin(self):
        if self._paths == []:
            exit(1)
        sys('clear')
        print(self._lvlMsg)
        self.enter_room()

        if self._lvlMsg2 != "":
            print(self._lvlMsg2)
        self._visits += 1
        return self.user_select()


class BossLevel(Level):

    """Child to class Level, establishes the functions,
    traits, and subsequent level to this particular level"""

    def __init__(self, hero, *nxt_lvl):
        super().__init__(hero)
        monster_boss = ch.Monster()
        monster_boss._health *= 2
        monster_boss._level *= 2
        self._monsters.append(monster_boss)

        self._lvlMsg = "\n\tFINALLY, you have found the Golden Router!"
        for level in nxt_lvl:
            self._paths.append(level)

    def begin(self):
        if self._paths == []:
            exit(1)
        print(self._lvlMsg)
        self.enter_room()

        ending = "Now that you have defeated the monster and collected"
        ending += " the Golden router, lets get out of here!"
        print(ending)
        self._visits += 1
        return self.user_select()
